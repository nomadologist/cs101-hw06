//Class for defining a paddle to be used in pong
public class Paddle
{
//	Variable declarations
	private double screenWidth;
	private double screenHeight;
	
	private int paddleWidth = 100;
	private int paddleHeight = 50;
	
	private int xPos;
	private int yPos;
	
	private int speed = 3;
	
//	Creates a paddle based on input screen width and height.
	public Paddle(double width, double height)
	{
		screenWidth = width;
		screenHeight = height;
		xPos = (int) (width/2);
		yPos = 480;
	}
//	Methods for returning characteristics of the paddle.
	public int getPaddleWidth()
	{
		return paddleWidth;
	}
	
	public int getPaddleHeight()
	{
		return paddleHeight;
	}
	
	public int getXPos()
	{
		return xPos;
	}
	
	public int getYPos()
	{
		return yPos;
	}
//	Moves paddle position left until edge of the screen is hit
	public void moveLeft()
	{
		if(xPos > 0)
			xPos -= speed;
	}
//	Moves paddle position right until edge of the screen is hit.
	public void moveRight()
	{
		if(xPos < (screenWidth - paddleWidth))
			xPos += speed;
	}
}
