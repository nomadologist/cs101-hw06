//Testing the Sanitizer class to see if it can remove the characters specified.
public interface SanitizerTest
{
	public static void main(String[] arg)
	{
//		If the sanitizer is successful should print hello world
		String test = "he#l*l!o %%wo$rl&&&d";
		System.out.println(Sanitizer.sanitize(test));
	}
}
