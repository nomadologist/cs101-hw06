//Finds the average of user inputted values that are separated by commas
import java.util.Scanner;

public class CommaSeparatedScores
{
	public static void main(String[] args)
	{
//		Gets values from user
		System.out.println("Please enter the string of scores, separated by commas");
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		in.close();
//		Splits them into an array using the comma as a delimiter
		String[] numbers = input.split(",");
		double[] result = new double[numbers.length];
		double total = 0;
//		Converts all the strings to doubles and adds them together
		for(int i = 0; i < numbers.length; i++)
		{
			result[i] = Double.parseDouble(numbers[i]);
			total+=result[i];
		}
//		finds the average of the numbers.
		System.out.println("The average is " + (total/result.length));
	}
}
