//Creates an instance of the MyTimer class and adds displays the time in milliseconds every iterations while adding numbers from 0-99
public class MyTimerTest
{
	public static void main(String[] args)
	{
		int num = 0;
		MyTimer timer = new MyTimer();
		timer.start();
		for(int i = 0; i<99; i++)
		{
			num++;
			System.out.println(timer.getElapsedTimeMillis());
		}
		timer.stop();
	}
}
