import processing.core.PApplet;

public class Pong extends PApplet
{
	Paddle paddle = new Paddle(500, 500);
	public static void main(String[] args)
	{
		PApplet.main("Pong");
	}
	
	public void settings()
	{
		size(500, 500);
	}
	
	public void setup()
	{
		
	}
	
	public void draw()
	{
//		Creates a rectangle based on generated paddle.
		background(0);
		rect(paddle.getXPos(), paddle.getYPos(), paddle.getPaddleWidth(), paddle.getPaddleHeight());
//		Moves paddle left or right depending on key presses.
		if(key == 'd')
			paddle.moveRight();
		if(key == 'a')
			paddle.moveLeft();
	}
}
